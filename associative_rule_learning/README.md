# Association Rule Learning

Association rule learning is a rule based machine learning method used to identify relation between variables in the form of rules. Such rules are accompnied by measures such as life, confidence, support and conviction.

# Contents

`associationrule.py` - Model that generates rules and associated metrics for input data.
##### Parameters
* s3endpointUrl - Endpoint to s3 storage backend
* s3accessKey - Access key to s3 storage backend
* s3secretKey - Secret key to s3 storage backend
* s3objectStoreLocation - Bucket name in s3 backend
* s3Path - object key containing training data (location in the bucket)
* destination - location to store generated rules (object key including filename)

# Workflow

## Save Data

Sample data (in spreadsheet format) used in this analysis can be found under 'data' folder. This data is from openstack-neutron team and shows developer and qe response to bugs in terms of time taken to fix bugs and time taken to verify bugs. For demonstration of this model, new data was constructed on developer and qe response timelines (on weekly basis) and then association rule learning model was executed to understand developers and qe response to various timelines. Since the model requires input to be in parquet format, data from spreadsheet needs to converted and stored in parquet format in the object storage.

## Run Model

#### Rule Learning

    curl -v --header "Authorization: Bearer $TOKEN" \
    http://seldon-core-apiserver-panbalag.apps.cluster-dc86.dc86.openshiftworkshop.com/api/v0.1/predictions -d \
    '{"strData":"s3endpointUrl='"$S3ENDPOINTURL"', s3accessKey='"$S3ACCESSKEY"', \
     s3secretKey='"$S3SECRETKEY"', s3objectStoreLocation=DH-DEV-DATA, s3Path=2018-01-01/rule_data, \
     destination=/associative_rule_learning/rules.csv"}' \
     -H "Content-Type: application/json"

## Use Results

Sample results from the rules generated has been shown below,

#### Developer vs Confidence level on whether the developer will be able to fix a high-severity or a urgent priority bug in a week

    TimRozet        1.00
    StevenHillman	0.80
    MikeKolesnik	0.50
    MiguelAngelAjo	0.43
    JohnSchwarz     0.40
    JakubLibosvar	0.36
    JonSchlueter	0.33
    AssafMuller     0.29
    TerryWilson     0.25
    IharHrachyshka  0.21
    anilvenkata     0.17
    BrentEagles     0.17
    NirMagnezi      0.13
    BrianHaley      0.13
    DanielAlvarez   0.07
    SlawekKaplonski	0.07
            
#### Severity of bugs vs Confidence level of QE engineers in verifying fix for the bugs within a week.

    high-Severity	0.68
    urgent-Severity	0.66

#### Severity of bugs vs Confidence level of developers in fixing the bugs in fixing the bug on weekly basis.

    urgent-Severity	1-week	0.31
    high-Severity	1-week	0.18
    high-Severity	2-week	0.10
    high-Severity	3-week	0.11
